package presentation.GUI;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.Font;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
/**
 * @author  Zaharie Andrei
 * @group: 30229
 * @Since: 11.04.2022
 * @Class: MainMenu - Reprezinta meniul principal din interfata grafica
 */
public class MainMenu extends JFrame {

    private JPanel contentPane;
    private JLabel mainMenuLabel;
    private JButton clientsButton;
    private JButton productsButton;
    private JButton ordersButton;


    public MainMenu() {
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setBounds(500, 100, 872, 346);
        contentPane = new JPanel();
        contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
        setContentPane(contentPane);
        contentPane.setLayout(null);

        mainMenuLabel = new JLabel("Main Menu");
        mainMenuLabel.setFont(new Font("Times New Roman", Font.BOLD, 24));
        mainMenuLabel.setBounds(342, 11, 131, 40);
        contentPane.add(mainMenuLabel);

        clientsButton = new JButton("Edit Clients");

        clientsButton.setBounds(55, 173, 131, 62);
        contentPane.add(clientsButton);

        productsButton = new JButton("Edit Products");
        productsButton.setBounds(342, 173, 131, 62);
        contentPane.add(productsButton);

        ordersButton = new JButton("Edit Orders");
        ordersButton.setBounds(657, 173, 131, 62);
        contentPane.add(ordersButton);
    }

    public void editClientsButtonListener(ActionListener actionListener){
        this.clientsButton.addActionListener(actionListener);
    }
    public void editProductsButtonListener(ActionListener actionListener){
        this.productsButton.addActionListener(actionListener);
    }
    public void editOrdersButtonListener(ActionListener actionListener){
        this.ordersButton.addActionListener(actionListener);
    }

}