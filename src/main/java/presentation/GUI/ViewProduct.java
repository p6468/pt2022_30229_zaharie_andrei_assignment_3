package presentation.GUI;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.Font;
import javax.swing.border.LineBorder;
import java.awt.Color;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.DefaultTableModel;
import javax.swing.border.TitledBorder;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
/**
 * @author  Zaharie Andrei
 * @group: 30229
 * @Since: 11.04.2022
 * @Class: ViewProduct - Reprezinta interfata grafica pentru pagina Product.
 */
public class ViewProduct extends JFrame {

    private JPanel contentPane;
    private JTextField productNameField;
    private JTextField producerField;
    private JTextField productPriceField;
    private JTextField productStockField;
    private JTable productsTable;
    private JButton homeButton;
    private JLabel viewClientLabel;
    private JLabel productInfoLabel;
    private JLabel idProductLabel;
    private JLabel productNameLabel;
    private JLabel productProducerLabel;
    private JLabel productPriceLabel;
    private JLabel productStockLabel;
    private JButton addProductButton;
    private JButton deleteProductButton;
    private JButton updateUpdateButton;
    private JButton searchProductButton;
    private JScrollPane scrollPane;
    private DefaultTableModel model;
    private ListSelectionModel modelSelection;
    private JTextField productIDField;




    public ViewProduct() {
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setBounds(500, 100, 864, 645);
        contentPane = new JPanel();
        contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
        setContentPane(contentPane);
        contentPane.setLayout(null);

        viewClientLabel = new JLabel("Clients");
        viewClientLabel.setFont(new Font("Times New Roman", Font.BOLD, 25));
        viewClientLabel.setBounds(373, 11, 75, 56);
        contentPane.add(viewClientLabel);

        productInfoLabel = new JLabel("Product Informations");
        productInfoLabel.setFont(new Font("Times New Roman", Font.PLAIN, 18));
        productInfoLabel.setBounds(69, 66, 180, 38);
        contentPane.add(productInfoLabel);

        idProductLabel = new JLabel("Product ID");
        idProductLabel.setFont(new Font("Times New Roman", Font.PLAIN, 12));
        idProductLabel.setBounds(21, 127, 89, 20);
        contentPane.add(idProductLabel);

        productNameLabel = new JLabel("Product Name:");
        productNameLabel.setFont(new Font("Times New Roman", Font.PLAIN, 13));
        productNameLabel.setBounds(21, 158, 89, 26);
        contentPane.add(productNameLabel);

        productProducerLabel = new JLabel("Producer:");
        productProducerLabel.setFont(new Font("Times New Roman", Font.PLAIN, 13));
        productProducerLabel.setBounds(21, 196, 89, 26);
        contentPane.add(productProducerLabel);

        productPriceLabel = new JLabel("Product Price");
        productPriceLabel.setFont(new Font("Times New Roman", Font.PLAIN, 13));
        productPriceLabel.setBounds(21, 233, 89, 26);
        contentPane.add(productPriceLabel);

        productStockLabel = new JLabel("Product Stock");
        productStockLabel.setFont(new Font("Times New Roman", Font.PLAIN, 13));
        productStockLabel.setBounds(21, 270, 89, 26);
        contentPane.add(productStockLabel);

        productNameField = new JTextField();
        productNameField.setBounds(131, 161, 118, 20);
        contentPane.add(productNameField);
        productNameField.setColumns(10);

        producerField = new JTextField();
        producerField.setColumns(10);
        producerField.setBounds(131, 199, 118, 20);
        contentPane.add(producerField);

        productPriceField = new JTextField();
        productPriceField.setColumns(10);
        productPriceField.setBounds(131, 236, 118, 20);
        contentPane.add(productPriceField);

        productStockField = new JTextField();
        productStockField.setColumns(10);
        productStockField.setBounds(131, 273, 118, 20);
        contentPane.add(productStockField);

        addProductButton = new JButton("Add Product");
        addProductButton.setBounds(21, 380, 118, 23);
        contentPane.add(addProductButton);

        deleteProductButton = new JButton("Delete Product");
        deleteProductButton.setBounds(21, 427, 118, 23);
        contentPane.add(deleteProductButton);

        updateUpdateButton = new JButton("Edit Product");
        updateUpdateButton.setBounds(169, 427, 118, 23);
        contentPane.add(updateUpdateButton);

        searchProductButton = new JButton("Search Product");
        searchProductButton.setBounds(169, 380, 118, 23);
        contentPane.add(searchProductButton);

        Object column[]= new Object[]{"Prod ID", "Prod Name", "Producer", "Prod Price", "Stock"};
        model = new DefaultTableModel();
        model.setColumnIdentifiers(column);

        productsTable = new JTable();
        productsTable.setModel(model);
        productsTable.getColumnModel().getColumn(0).setPreferredWidth(51);
        productsTable.getColumnModel().getColumn(1).setPreferredWidth(114);
        productsTable.getColumnModel().getColumn(2).setPreferredWidth(116);


        productsTable.setFont(new Font("Times New Roman", Font.PLAIN, 15));
        productsTable.setSurrendersFocusOnKeystroke(true);


        scrollPane = new JScrollPane(productsTable);
        productsTable.setFillsViewportHeight(true);
        scrollPane.setBounds(344, 78, 463, 489);
        contentPane.add(scrollPane);

        homeButton = new JButton("Back To Main Menu");
        homeButton.setBounds(21, 550, 155, 23);
        contentPane.add(homeButton);

        productIDField = new JTextField();
        productIDField.setColumns(10);
        productIDField.setBounds(131, 127, 118, 20);
        contentPane.add(productIDField);
        modelSelection = productsTable.getSelectionModel();
    }

    /**
     *
     * @param actionListener
     */
    public void mainMenuButtonListener(ActionListener actionListener){
        this.homeButton.addActionListener(actionListener);
    }

    /**
     *
     * @param actionListener
     */
    public void addSelectionListener(ListSelectionListener actionListener){
        this.modelSelection.addListSelectionListener(actionListener);
    }

    /**
     *
     * @return String
     */
    public String getProductName() {
        return productNameField.getText();
    }

    /**
     *
     * @return String
     */
    public String getProductProducer() {
        return producerField.getText();
    }
    /**
     *
     * @return String
     */
    public String getProductPrice() {
        return productPriceField.getText();
    }
    /**
     *
     * @return String
     */
    public String getProductStock() {
        return productStockField.getText();
    }

    public void setIdProductLabel(String id){
        this.idProductLabel.setText("Product id: "+id);
    }

    /**
     *
     * @return JTable
     */
    public JTable getProductTable() {
        return productsTable;
    }

    /**
     *
     * @param actionListener
     */
    public void insertButtonListener(ActionListener actionListener){
        this.addProductButton.addActionListener(actionListener);
    }

    /**
     *
     * @param actionListener
     */
    public void deleteProductButton(ActionListener actionListener){
        this.deleteProductButton.addActionListener(actionListener);
    }

    /**
     *
     * @param actionListener
     */
    public void updateProductButton(ActionListener actionListener){
        this.updateUpdateButton.addActionListener(actionListener);
    }

    /**
     *
     * @param actionListener
     */
    public void searchProductButton(ActionListener actionListener){
        this.searchProductButton.addActionListener(actionListener);
    }

    /**
     *
     * @return DefaultTableModel
     */
    public DefaultTableModel getModel() {
        return model;
    }

    /**
     *
     * @return DefaultTableModel
     */
    public ListSelectionModel getModelSelection() {
        return modelSelection;
    }

    /**
     *
     * @return String
     */
    public String getProductId(){
        return this.productIDField.getText();
    }

    /**
     *
     * @param productNameField
     */
    public void setProductNameField(String productNameField) {
        this.productNameField.setText(productNameField);
    }

    /**
     *
     * @param producerField
     */
    public void setProducerField(String producerField) {
        this.producerField.setText(producerField);
    }

    /**
     *
     * @param productPriceField
     */
    public void setProductPriceField(String productPriceField) {
        this.productPriceField.setText(productPriceField);
    }

    /**
     *
     * @param productStockField
     */
    public void setProductStockField(String productStockField) {
        this.productStockField.setText(productStockField);
    }

    /**
     *
     * @param productIDField
     */
    public void setProductIDField(String productIDField) {
        this.productIDField.setText(productIDField);
    }

    public void showErrorMessage(String mesaj){
        JOptionPane.showMessageDialog(this, mesaj);
    }

}
