package presentation.GUI;


import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.DefaultTableModel;
import java.awt.Font;
import java.awt.Component;
import java.awt.event.ActionListener;
/**
 * @author  Zaharie Andrei
 * @group: 30229
 * @Since: 11.04.2022
 * @Class: ViewOrder - Reprezinta interfata grafica pentru pagina Order.
 */
public class ViewOrder extends JFrame {

    private JPanel contentPane;
    private JTable clientsTable;
    private JTable productsTable;
    private JScrollPane scrollPaneProducts;
    private JScrollPane scrollPaneClients;
    private JScrollPane scrollPaneOrders;
    private JButton addOrderButton;
    private JButton mainMenuButton;
    private JLabel viewOrdersLabel;
    private JButton generatePDFButton;
    private JTable ordersTable;
    private JTextField cantitateField;
    private DefaultTableModel modelProduct;
    private ListSelectionModel modelSelectionProduct;
    private DefaultTableModel modelClient;
    private ListSelectionModel modelSelectionClient;
    private DefaultTableModel modelOrder;
    private ListSelectionModel modelSelectionOrder;
    private JTextField searchField;
    private JButton deleteOrderButton;
    private  JButton searchButton;


    public ViewOrder() {
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setBounds(500, 100, 1065, 630);
        contentPane = new JPanel();
        contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
        setContentPane(contentPane);
        contentPane.setLayout(null);

        viewOrdersLabel = new JLabel("Orders ");
        viewOrdersLabel.setBounds(500, 0, 85, 51);
        viewOrdersLabel.setFont(new Font("Times New Roman", Font.BOLD, 26));
        contentPane.add(viewOrdersLabel);

        addOrderButton = new JButton("Add Order");
        addOrderButton.setBounds(22, 406, 126, 23);
        contentPane.add(addOrderButton);

        mainMenuButton = new JButton("Back to Main Menu");
        mainMenuButton.setBounds(22, 555, 159, 23);
        contentPane.add(mainMenuButton);

        Object columnClients[]= new Object[]{"Client ID", "Client Name", "Client Address", "Email", "Phone Nr.", "Age"};
        modelClient = new DefaultTableModel();
        modelClient.setColumnIdentifiers(columnClients);
        clientsTable = new JTable();
        clientsTable.setModel(modelClient);
        clientsTable.getColumnModel().getColumn(0).setPreferredWidth(51);
        clientsTable.getColumnModel().getColumn(1).setPreferredWidth(94);
        clientsTable.getColumnModel().getColumn(2).setPreferredWidth(99);
        clientsTable.getColumnModel().getColumn(3).setPreferredWidth(174);
        clientsTable.getColumnModel().getColumn(4).setPreferredWidth(87);
        clientsTable.getColumnModel().getColumn(5).setPreferredWidth(30);
        clientsTable.setFillsViewportHeight(true);


        clientsTable.setFont(new Font("Times New Roman", Font.PLAIN, 14));
        clientsTable.setSurrendersFocusOnKeystroke(true);


        scrollPaneClients = new JScrollPane(clientsTable);
        clientsTable.setFillsViewportHeight(true);
        scrollPaneClients.setBounds(10, 59, 514, 293);
        contentPane.add(scrollPaneClients);


        Object columnProduct[]= new Object[]{"Prod ID", "Prod Name", "Producer", "Prod Price", "Stock"};
        modelProduct = new DefaultTableModel();
        modelProduct.setColumnIdentifiers(columnProduct);

        productsTable = new JTable();
        productsTable.setModel(modelProduct);
        productsTable.getColumnModel().getColumn(0).setPreferredWidth(51);
        productsTable.getColumnModel().getColumn(1).setPreferredWidth(114);
        productsTable.getColumnModel().getColumn(2).setPreferredWidth(116);


        productsTable.setFont(new Font("Times New Roman", Font.PLAIN, 14));
        productsTable.setSurrendersFocusOnKeystroke(true);
        scrollPaneProducts = new JScrollPane(productsTable);
        productsTable.setFillsViewportHeight(true);
        scrollPaneProducts.setBounds(565, 59, 463, 293);
        contentPane.add(scrollPaneProducts);

        generatePDFButton = new JButton("Generate PDF");
        generatePDFButton.setBounds(22, 452, 126, 23);
        contentPane.add(generatePDFButton);


        Object columnOrder[]= new Object[]{ "Order ID", "Client ID", "Product ID", "Cantitate", "Total Price"};
        modelOrder = new DefaultTableModel();
        modelOrder.setColumnIdentifiers(columnOrder);
        ordersTable = new JTable();
        ordersTable.setModel(modelOrder);

        scrollPaneOrders = new JScrollPane(ordersTable);
        scrollPaneOrders.setBounds(264, 363, 575, 215);
        contentPane.add(scrollPaneOrders);

        modelSelectionClient = clientsTable.getSelectionModel();
        modelSelectionOrder = ordersTable.getSelectionModel();
        modelSelectionProduct = productsTable.getSelectionModel();

        JLabel cantitateLabel = new JLabel("Cantitate:");
        cantitateLabel.setFont(new Font("Times New Roman", Font.BOLD, 14));
        cantitateLabel.setBounds(22, 380, 73, 14);
        contentPane.add(cantitateLabel);

        cantitateField = new JTextField();
        cantitateField.setText("1");
        cantitateField.setFont(new Font("Times New Roman", Font.BOLD, 14));
        cantitateField.setBounds(83, 375, 65, 20);
        contentPane.add(cantitateField);
        cantitateField.setColumns(10);

        deleteOrderButton = new JButton("Delete Order");
        deleteOrderButton.setBounds(849, 452, 179, 23);
        contentPane.add(deleteOrderButton);

        searchButton = new JButton("Search Order By ID");
        searchButton.setBounds(849, 406, 136, 23);
        contentPane.add(searchButton);

        searchField = new JTextField();
        searchField.setBounds(990, 407, 38, 20);
        contentPane.add(searchField);
        searchField.setColumns(10);
    }

    /**
     *
     * @return String
     */
    public String getCatitateField(){
        return this.cantitateField.getText();
    }

    /**
     *
     * @param actionListener
     */
    public void mainMenuButtonListener(ActionListener actionListener){
        this.mainMenuButton.addActionListener(actionListener);
    }

    /**
     *
     * @param actionListener
     */
    public void addOrderButtonListener(ActionListener actionListener){
        this.addOrderButton.addActionListener(actionListener);
    }

    /**
     *
     * @param actionListener
     */
    public void generatePDFButtonListener(ActionListener actionListener) {
        this.generatePDFButton.addActionListener(actionListener);
    }

    /**
     *
     * @param selectionListener
     */
    public void clientSelectionListener(ListSelectionListener selectionListener){
        this.modelSelectionClient.addListSelectionListener(selectionListener);
    }

    /**
     *
     * @param selectionListener
     */
    public void productSelectionListener(ListSelectionListener selectionListener){
        this.modelSelectionProduct.addListSelectionListener(selectionListener);
    }

    /**
     *
     * @param selectionListener
     */
    public void orderSelectionListener(ListSelectionListener selectionListener){
        this.modelSelectionOrder.addListSelectionListener(selectionListener);
    }

    /**
     *
     * @param actionListener
     */
    public void searchOrderButtonListener(ActionListener actionListener){
        this.searchButton.addActionListener(actionListener);
    }

    /**
     *
     * @param actionListener
     */
    public void deleteOrderButtonListener(ActionListener actionListener){
        this.deleteOrderButton.addActionListener(actionListener);
    }

    /**
     *
     * @return String
     */
    public String getSearchId(){
        return this.searchField.getText();
    }

    /**
     *
     * @param mesaj
     */
    public void showErrorMessage(String mesaj){
        JOptionPane.showMessageDialog(this, mesaj);
    }

    /**
     *
     * @return DefaultTableModel
     */
    public DefaultTableModel getModelProduct() {
        return modelProduct;
    }
    /**
     *
     * @return DefaultTableModel
     */
    public DefaultTableModel getModelClient() {
        return modelClient;
    }
    /**
     *
     * @return DefaultTableModel
     */
    public DefaultTableModel getModelOrder() {
        return modelOrder;
    }

    /**
     *
     * @return JTable
     */
    public JTable getClientsTable() {
        return clientsTable;
    }
    /**
     *
     * @return JTable
     */
    public JTable getProductsTable() {
        return productsTable;
    }
    /**
     *
     * @return JTable
     */
    public JTable getOrdersTable() {
        return ordersTable;
    }

    /**
     *
     * @return ListSelectionModel
     */
    public ListSelectionModel getModelSelectionProduct() {
        return modelSelectionProduct;
    }
    /**
     *
     * @return ListSelectionModel
     */
    public ListSelectionModel getModelSelectionClient() {
        return modelSelectionClient;
    }
    /**
     *
     * @return ListSelectionModel
     */
    public ListSelectionModel getModelSelectionOrder() {
        return modelSelectionOrder;
    }
}
