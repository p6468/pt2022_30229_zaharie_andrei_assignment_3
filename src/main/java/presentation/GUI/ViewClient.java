package presentation.GUI;
import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.Font;
import javax.swing.border.LineBorder;
import java.awt.Color;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.DefaultTableModel;
import javax.swing.border.TitledBorder;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
/**
 * @author  Zaharie Andrei
 * @group: 30229
 * @Since: 11.04.2022
 * @Class: ViewClient - Reprezinta interfata grafica pentru pagina Client.
 */
public class ViewClient extends JFrame {

    private JPanel contentPane;
    private JTextField clientNameField;
    private JTextField clientAddressField;
    private JTextField emailAddressField;
    private JTextField phoneNumberField;
    private JTextField clientAgeField;
    private JTable clientsTable;
    private JButton homeButton;
    private JLabel viewClientLabel;
    private JLabel clientInfoLabel;
    private JLabel idClientLabel;
    private JTextField clientIdField;
    private JLabel clientNameLabel;
    private JLabel clientAddressLabel;
    private JLabel emailAddressLabel;
    private JLabel phoneNumberLabel;
    private JLabel clientAgeLabel;
    private JButton addClientButton;
    private JButton deleteClientButton;
    private JButton updateClientButton;
    private JButton searchClientButton;
    private JScrollPane scrollPane;
    private DefaultTableModel model;
    private ListSelectionModel modelSelection;


    public ViewClient() {
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setBounds(500, 100, 862, 649);
        contentPane = new JPanel();
        contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
        setContentPane(contentPane);
        contentPane.setLayout(null);

        viewClientLabel = new JLabel("Clients");
        viewClientLabel.setFont(new Font("Times New Roman", Font.BOLD, 25));
        viewClientLabel.setBounds(373, 11, 75, 56);
        contentPane.add(viewClientLabel);

        clientInfoLabel = new JLabel("Client Informations");
        clientInfoLabel.setFont(new Font("Times New Roman", Font.PLAIN, 18));
        clientInfoLabel.setBounds(70, 74, 148, 38);
        contentPane.add(clientInfoLabel);

        idClientLabel = new JLabel("Client ID:");
        idClientLabel.setFont(new Font("Times New Roman", Font.PLAIN, 12));
        idClientLabel.setBounds(26, 123, 70, 14);
        contentPane.add(idClientLabel);

        clientNameLabel = new JLabel("Client Name:");
        clientNameLabel.setFont(new Font("Times New Roman", Font.PLAIN, 13));
        clientNameLabel.setBounds(21, 158, 75, 26);
        contentPane.add(clientNameLabel);

        clientAddressLabel = new JLabel("Client Address:");
        clientAddressLabel.setFont(new Font("Times New Roman", Font.PLAIN, 13));
        clientAddressLabel.setBounds(21, 196, 89, 26);
        contentPane.add(clientAddressLabel);

        emailAddressLabel = new JLabel("Email Address:");
        emailAddressLabel.setFont(new Font("Times New Roman", Font.PLAIN, 13));
        emailAddressLabel.setBounds(21, 233, 89, 26);
        contentPane.add(emailAddressLabel);

        phoneNumberLabel = new JLabel("Phone Number:");
        phoneNumberLabel.setFont(new Font("Times New Roman", Font.PLAIN, 13));
        phoneNumberLabel.setBounds(21, 270, 89, 26);
        contentPane.add(phoneNumberLabel);

        clientAgeLabel = new JLabel("Client Age:");
        clientAgeLabel.setFont(new Font("Times New Roman", Font.PLAIN, 13));
        clientAgeLabel.setBounds(21, 307, 75, 26);
        contentPane.add(clientAgeLabel);

        clientNameField = new JTextField();
        clientNameField.setBounds(131, 161, 118, 20);
        contentPane.add(clientNameField);
        clientNameField.setColumns(10);

        clientAddressField = new JTextField();
        clientAddressField.setColumns(10);
        clientAddressField.setBounds(131, 199, 118, 20);
        contentPane.add(clientAddressField);

        clientIdField = new JTextField();
        clientIdField.setBounds(131, 123, 118, 20);
        contentPane.add(clientIdField);

        emailAddressField = new JTextField();
        emailAddressField.setColumns(10);
        emailAddressField.setBounds(131, 236, 118, 20);
        contentPane.add(emailAddressField);

        phoneNumberField = new JTextField();
        phoneNumberField.setColumns(10);
        phoneNumberField.setBounds(131, 273, 118, 20);
        contentPane.add(phoneNumberField);

        clientAgeField = new JTextField();
        clientAgeField.setColumns(10);
        clientAgeField.setBounds(131, 310, 118, 20);
        contentPane.add(clientAgeField);

        addClientButton = new JButton("Add Client");
        addClientButton.setBounds(21, 380, 118, 23);
        contentPane.add(addClientButton);

        deleteClientButton = new JButton("Delete Client");
        deleteClientButton.setBounds(21, 427, 118, 23);
        contentPane.add(deleteClientButton);

        updateClientButton = new JButton("Edit Client");
        updateClientButton.setBounds(169, 427, 118, 23);
        contentPane.add(updateClientButton);

        searchClientButton = new JButton("Search Client");
        searchClientButton.setBounds(169, 380, 118, 23);
        contentPane.add(searchClientButton);

        Object column[]= new Object[]{"Client ID", "Client Name", "Client Address", "Email", "Phone Nr.", "Age"};
        model = new DefaultTableModel();
        model.setColumnIdentifiers(column);
        clientsTable = new JTable();
        clientsTable.setModel(model);
        clientsTable.getColumnModel().getColumn(0).setPreferredWidth(51);
        clientsTable.getColumnModel().getColumn(1).setPreferredWidth(94);
        clientsTable.getColumnModel().getColumn(2).setPreferredWidth(99);
        clientsTable.getColumnModel().getColumn(3).setPreferredWidth(174);
        clientsTable.getColumnModel().getColumn(4).setPreferredWidth(87);
        clientsTable.getColumnModel().getColumn(5).setPreferredWidth(30);
        clientsTable.setFillsViewportHeight(true);


        clientsTable.setFont(new Font("Times New Roman", Font.PLAIN, 14));
        clientsTable.setSurrendersFocusOnKeystroke(true);


        scrollPane = new JScrollPane(clientsTable);
        clientsTable.setFillsViewportHeight(true);
        scrollPane.setBounds(308, 99, 528, 449);
        contentPane.add(scrollPane);

        homeButton = new JButton("Back To Main Menu");
        homeButton.setBounds(21, 550, 150, 23);
        contentPane.add(homeButton);
        modelSelection = clientsTable.getSelectionModel();
    }

    /**
     *
     * @param actionListener
     */
    public void mainMenuButtonListener(ActionListener actionListener){
        this.homeButton.addActionListener(actionListener);
    }

    /**
     *
     * @param actionListener
     */
    public void addSelectionListener(ListSelectionListener actionListener){
        this.modelSelection.addListSelectionListener(actionListener);
    }
    public String getIdClientLabel(){
        return this.idClientLabel.getText();
    }

    /**
     *
     * @return String
     */
    public String getClientName() {
        return clientNameField.getText();
    }

    /**
     *
     * @return String
     */
    public String getClientAddress() {
        return clientAddressField.getText();
    }

    /**
     *
     * @return String
     */
    public String getEmailAddress() {
        return emailAddressField.getText();
    }

    /**
     *
     * @return String
     */
    public String getPhoneNumber() {
        return phoneNumberField.getText();
    }

    /**
     *
     * @return String
     */
    public String getClientAge() {
        return clientAgeField.getText();
    }

    /**
     *
     * @param clientNameLabel
     */
    public void setClientNameField(String clientNameLabel) {
        this.clientNameField.setText(clientNameLabel);
    }

    /**
     *
     * @param clientAddressLabel
     */
    public void setClientAddressField(String clientAddressLabel) {
        this.clientAddressField.setText(clientAddressLabel);
    }

    /**
     *
     * @param emailAddressLabel
     */
    public void setEmailAddressField(String emailAddressLabel) {
        this.emailAddressField.setText( emailAddressLabel);
    }

    /**
     *
     * @param phoneNumberLabel
     */
    public void setPhoneNumberField(String phoneNumberLabel) {
        this.phoneNumberField.setText(phoneNumberLabel);
    }

    /**
     *
     * @param clientAgeLabel
     */
    public void setClientAgeField(String clientAgeLabel) {
        this.clientAgeField.setText(clientAgeLabel);
    }

    /**
     *
     * @param id
     */
    public void setIdClientField(String id){
        this.idClientLabel.setText("Client ID: "+id);
    }

    /**
     *
     * @param id
     */
    public void setIdClientText(String id){
        this.clientIdField.setText(""+id);
    }

    /**
     *
     * @return
     */
    public String getClientID(){
        return clientIdField.getText();
    }

    /**
     *
     * @return
     */
    public JTable getClientsTable() {
        return clientsTable;
    }

    /**
     *
     * @param actionListener
     */
    public void insertButtonListener(ActionListener actionListener){
        this.addClientButton.addActionListener(actionListener);
    }

    /**
     *
     * @param actionListener
     */
    public void deleteClientButton(ActionListener actionListener){
        this.deleteClientButton.addActionListener(actionListener);
    }

    /**
     *
     * @param actionListener
     */
    public void updateClientButton(ActionListener actionListener){
        this.updateClientButton.addActionListener(actionListener);
    }

    /**
     *
     * @param actionListener
     */
    public void searchClientButton(ActionListener actionListener){
        this.searchClientButton.addActionListener(actionListener);
    }

    /**
     *
     * @return DefaultTableModel
     */
    public DefaultTableModel getModel() {
        return model;
    }

    /**
     *
     * @return ListSelectionModel
     */
    public ListSelectionModel getModelSelection() {
        return modelSelection;
    }
    public void showErrorMessage(String mesaj){
        JOptionPane.showMessageDialog(this, mesaj);
    }
}

