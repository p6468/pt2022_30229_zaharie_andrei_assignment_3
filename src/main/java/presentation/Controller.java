package presentation;

import businessLogic.ClientBLL;
import businessLogic.OrdersBLL;
import businessLogic.ProductBLL;
import model.Client;
import model.Orders;
import model.Product;
import presentation.GUI.MainMenu;
import presentation.GUI.ViewClient;
import presentation.GUI.ViewOrder;
import presentation.GUI.ViewProduct;

import javax.swing.table.DefaultTableModel;
/**
 * @author Zaharie Andrei
 * @group: 30229
 * @Since: 11.04.2022
 * @Class: Controller - Reprezinta clasa responsabila pentru legatura dintre interfata grafica si aplicatie
 */
public class Controller {
    private MainMenu mainMenu;
    private ViewClient viewClient;
    private ViewProduct viewProduct;
    private ViewOrder viewOrder;
    private ProductBLL productBLL;
    private ClientBLL clientBLL;
    private OrdersBLL ordersBLL;

    public Controller(){
        this.mainMenu = new MainMenu();
        this.viewClient = new ViewClient();
        this.viewProduct = new ViewProduct();
        this.viewOrder = new ViewOrder();
        this.productBLL = new ProductBLL();
        this.clientBLL = new ClientBLL();
        this.ordersBLL = new OrdersBLL();

        this.meniu();
        this.clientsOperations();
        this.productsOperations();
        this.ordersOperations();

    }
    public void meniu(){
        this.mainMenu.editClientsButtonListener(e->{
            openClients();
        });
        this.mainMenu.editProductsButtonListener(e->{
            openProducts();
        });
        this.mainMenu.editOrdersButtonListener(e->{
            openOrders();
        });
        this.viewClient.mainMenuButtonListener(e->{
            openMainMenu();
        });
        this.viewOrder.mainMenuButtonListener(e->{
            openMainMenu();
        });
        this.viewProduct.mainMenuButtonListener(e->{
            openMainMenu();
        });
    }

    public void clientsOperations(){
        this.viewClient.addSelectionListener(e->{
            if(!this.viewClient.getModelSelection().isSelectionEmpty()){
                int selectedRow = this.viewClient.getModelSelection().getMinSelectionIndex();
                int idClient = Integer.parseInt(this.viewClient.getClientsTable().getValueAt(selectedRow,0).toString());
                String clientName = this.viewClient.getClientsTable().getValueAt(selectedRow,1).toString();
                String clientAddress =this.viewClient.getClientsTable().getValueAt(selectedRow,2).toString();
                String emailAddress = this.viewClient.getClientsTable().getValueAt(selectedRow,3).toString();
                String phoneNumber = this.viewClient.getClientsTable().getValueAt(selectedRow,4).toString();
                int age = Integer.parseInt(this.viewClient.getClientsTable().getValueAt(selectedRow,5).toString());

                this.viewClient.setIdClientText(""+idClient);
                this.viewClient.setIdClientField(""+idClient);
                this.viewClient.setClientNameField(clientName);
                this.viewClient.setClientAddressField(clientAddress);
                this.viewClient.setEmailAddressField(emailAddress);
                this.viewClient.setPhoneNumberField(phoneNumber);
                this.viewClient.setClientAgeField(""+age);
            }
                });
        this.viewClient.insertButtonListener(e->{
            Client newClient = this.getInfoClient();
            this.clientBLL.insertClient(newClient);
            DefaultTableModel model = clientBLL.getClientsTable(viewClient.getModel());
            viewClient.getClientsTable().setModel(model);
        });

        this.viewClient.updateClientButton(e->{
            Client newClient = this.getInfoClient();

            int id = Integer.parseInt(this.viewClient.getClientID());
            //System.out.println("ID GASIT "+id);
            newClient.setIdClient(id);

            this.clientBLL.updateClient(newClient);
            DefaultTableModel model = clientBLL.getClientsTable(viewClient.getModel());
            viewClient.getClientsTable().setModel(model);

        });
        this.viewClient.deleteClientButton(e->{

            int id = Integer.parseInt(this.viewClient.getClientID());
            this.clientBLL.deleteClient(id);
            DefaultTableModel model = clientBLL.getClientsTable(viewClient.getModel());
            viewClient.getClientsTable().setModel(model);
        });

        this.viewClient.searchClientButton(e->{

            int id = Integer.parseInt(this.viewClient.getClientID());
            DefaultTableModel model = this.clientBLL.searchClient(id, viewClient.getModel());
            viewClient.getClientsTable().setModel(model);
        });
    }

    public void productsOperations(){
        this.viewProduct.addSelectionListener(e->{
            if(!this.viewProduct.getModelSelection().isSelectionEmpty()){
                int selectedRow = this.viewProduct.getModelSelection().getMinSelectionIndex();
                int prodId = Integer.parseInt(this.viewProduct.getProductTable().getValueAt(selectedRow,0).toString());
                String prodName = this.viewProduct.getProductTable().getValueAt(selectedRow,1).toString();
                String producer =this.viewProduct.getProductTable().getValueAt(selectedRow,2).toString();
                double prodPrice = Double.parseDouble(this.viewProduct.getProductTable().getValueAt(selectedRow,3).toString());
                int prodStock = Integer.parseInt(this.viewProduct.getProductTable().getValueAt(selectedRow,4).toString());

                this.viewProduct.setProductIDField(""+prodId);
                this.viewProduct.setProductNameField(prodName);
                this.viewProduct.setProducerField(producer);
                this.viewProduct.setProductPriceField(""+prodPrice);
                this.viewProduct.setProductStockField(""+prodStock);

            }
        });
        this.viewProduct.insertButtonListener(e->{
            Product newProduct = getInfoProduct();
            this.productBLL.insertProduct(newProduct);
            DefaultTableModel model = productBLL.getProductTable(this.viewProduct.getModel());
            viewProduct.getProductTable().setModel(model);
        });

        this.viewProduct.updateProductButton(e->{
            Product newProduct = getInfoProduct();
            this.productBLL.updateProduct(newProduct);
            DefaultTableModel model = productBLL.getProductTable(this.viewProduct.getModel());
            viewProduct.getProductTable().setModel(model);

        });
        this.viewProduct.deleteProductButton(e->{
            int id = Integer.parseInt(this.viewProduct.getProductId());
            this.productBLL.deleteProduct(id);
            DefaultTableModel model = productBLL.getProductTable(this.viewProduct.getModel());
            viewProduct.getProductTable().setModel(model);

        });

        this.viewProduct.searchProductButton(e->{
            int id = Integer.parseInt(this.viewProduct.getProductId());
            DefaultTableModel model = this.productBLL.searchProduct(id,viewProduct.getModel());
            viewProduct.getProductTable().setModel(model);

        });


    }

    public void ordersOperations(){
        Client clientSelected = new Client();
        Product productSelected = new Product();
        Orders orderSelected = new Orders();

        this.viewOrder.clientSelectionListener(e->{
            if(!this.viewOrder.getModelSelectionClient().isSelectionEmpty()){
                int selectedRow =this.viewOrder.getModelSelectionClient().getMinSelectionIndex();
                int idClient = Integer.parseInt(this.viewOrder.getClientsTable().getValueAt(selectedRow,0).toString());
                String clientName = this.viewOrder.getClientsTable().getValueAt(selectedRow,1).toString();
                String clientAddress =this.viewOrder.getClientsTable().getValueAt(selectedRow,2).toString();
                String emailAddress = this.viewOrder.getClientsTable().getValueAt(selectedRow,3).toString();
                String phoneNumber = this.viewOrder.getClientsTable().getValueAt(selectedRow,4).toString();
                int age = Integer.parseInt(this.viewOrder.getClientsTable().getValueAt(selectedRow,5).toString());

                clientSelected.setIdClient(idClient);
                clientSelected.setClientName(clientName);
                clientSelected.setClientAddress(clientAddress);
                clientSelected.setEmailAddress(emailAddress);
                clientSelected.setPhoneNumber(phoneNumber);
                clientSelected.setAge(age);
            }
            else{
                clientSelected.setIdClient(-1);
            }
        });

        this.viewOrder.productSelectionListener(e->{
            if(!this.viewOrder.getModelSelectionProduct().isSelectionEmpty()){
                int selectedRow = this.viewOrder.getModelSelectionProduct().getMinSelectionIndex();
                int prodId = Integer.parseInt(this.viewOrder.getProductsTable().getValueAt(selectedRow,0).toString());
                String prodName = this.viewOrder.getProductsTable().getValueAt(selectedRow,1).toString();
                String producer = this.viewOrder.getProductsTable().getValueAt(selectedRow,2).toString();
                double prodPrice = Double.parseDouble(this.viewOrder.getProductsTable().getValueAt(selectedRow,3).toString());
                int prodStock = Integer.parseInt(this.viewOrder.getProductsTable().getValueAt(selectedRow,4).toString());

                productSelected.setIdProduct(prodId);
                productSelected.setProductName(prodName);
                productSelected.setProductProducer(producer);
                productSelected.setProductPrice(prodPrice);
                productSelected.setProductStock(prodStock);
            }
            else{
                productSelected.setIdProduct(-1);
            }
        });

        this.viewOrder.orderSelectionListener(e->{
            if(!this.viewOrder.getModelSelectionOrder().isSelectionEmpty()){
                int selectedRow = this.viewOrder.getModelSelectionOrder().getMinSelectionIndex();
                int orderId = Integer.parseInt(this.viewOrder.getOrdersTable().getValueAt(selectedRow,0).toString());
                int clientId = Integer.parseInt(this.viewOrder.getOrdersTable().getValueAt(selectedRow,1).toString());
                int productId = Integer.parseInt(this.viewOrder.getOrdersTable().getValueAt(selectedRow,2).toString());
                int cantitate = Integer.parseInt(this.viewOrder.getOrdersTable().getValueAt(selectedRow,3).toString());
                double totalPrice = Double.parseDouble(this.viewOrder.getOrdersTable().getValueAt(selectedRow,4).toString());

               orderSelected.setIdOrder(orderId);
               orderSelected.setIdClient(clientId);
               orderSelected.setIdProduct(productId);
               orderSelected.setTotalPrice(totalPrice);
               orderSelected.setCantitate(cantitate);
            }else{
                orderSelected.setIdOrder(-1);
            }
        });

        this.viewOrder.addOrderButtonListener(e->{
            try {
                int cantitate = Integer.parseInt(this.viewOrder.getCatitateField());


                if(productSelected.getIdProduct() == -1){
                    this.viewOrder.showErrorMessage("Nu ati selectat nici un produs!");
                }
                else if(clientSelected.getIdClient() == -1){
                    this.viewOrder.showErrorMessage("Nu ati selectat nici un client!");
                } else{
                    if(cantitate > productSelected.getProductStock()){
                        this.viewOrder.showErrorMessage("Cantitatea selectata este mai mare decat stocul!");
                    }else {
                        this.ordersBLL.addOrder(clientSelected, productSelected, cantitate);
                    }
                }
                DefaultTableModel modelOrder = ordersBLL.getOrdersTable(viewOrder.getModelOrder());
                viewOrder.getOrdersTable().setModel(modelOrder);

                DefaultTableModel modelProduct = productBLL.getProductTable(viewOrder.getModelProduct());
                viewOrder.getProductsTable().setModel(modelProduct);
            }catch(NumberFormatException ex){
                this.viewOrder.showErrorMessage("Cantitatea introdusa nu este un numar valid!");
            }

        });

        this.viewOrder.searchOrderButtonListener(e->{
            try{
                int id = Integer.parseInt(this.viewOrder.getSearchId());
                DefaultTableModel modelOrder = this.ordersBLL.searchOrder(id, this.viewOrder.getModelOrder());
                viewOrder.getOrdersTable().setModel(modelOrder);
            }catch(NumberFormatException ex){
                this.viewOrder.showErrorMessage("Idul introdus trebuie sa fie un numar!");
            }
        });

        this.viewOrder.deleteOrderButtonListener(e->{
            this.ordersBLL.deleteOrder(orderSelected.getIdOrder());
            DefaultTableModel modelOrder = ordersBLL.getOrdersTable(viewOrder.getModelOrder());
            viewOrder.getOrdersTable().setModel(modelOrder);
        });

        this.viewOrder.generatePDFButtonListener(e->{
            if(orderSelected.getIdOrder() < 0){
                viewOrder.showErrorMessage("Nu ati selectat nicio comanda pentru a putea genera un PDF!");
            }
            else {
                this.ordersBLL.generatePDF(orderSelected);
            }
        });
    }

    /**
     *
     * @return newClient
     */
    public Client getInfoClient(){
        String clientName = this.viewClient.getClientName();
        String clientAddress = this.viewClient.getClientAddress();
        String emailAddress = this.viewClient.getEmailAddress();
        String phoneNumber = this.viewClient.getPhoneNumber();
        int age = Integer.parseInt(this.viewClient.getClientAge());
        Client newClient = new Client(clientName,clientAddress,emailAddress,phoneNumber,age);
        System.out.println(newClient);
        return newClient;
    }

    /**
     *
     * @return newProduct
     */
    public Product getInfoProduct(){
        int idProduct = 0;
        if(!this.viewProduct.getProductId().equals("")) {
           idProduct = Integer.parseInt(this.viewProduct.getProductId());
        }
        String prodName = this.viewProduct.getProductName();
        String producer = this.viewProduct.getProductProducer();
        double prodPrice = Double.parseDouble(this.viewProduct.getProductPrice());
        int stock = Integer.parseInt(this.viewProduct.getProductStock());
        Product newProduct = new Product(idProduct,prodName,producer,prodPrice,stock);
        return newProduct;
    }

    /**
     * Deschide interfata Main Menu.
     */
    public void openMainMenu(){
        this.mainMenu.setVisible(true);
        this.viewProduct.setVisible(false);
        this.viewClient.setVisible(false);
        this.viewOrder.setVisible(false);
    }

    /**
     * Deschide interfata Clients si afiseaza informatiile necesare din baza de date in tabel.
     */
    public void openClients(){
        this.mainMenu.setVisible(false);
        this.viewProduct.setVisible(false);
        this.viewClient.setVisible(true);
        this.viewOrder.setVisible(false);
        DefaultTableModel model = clientBLL.getClientsTable(viewClient.getModel());
        viewClient.getClientsTable().setModel(model);
    }

    /**
     * Deschide interfata Products si afiseaza informatiile necesare din baza de date in tabel.
     */
    public void openProducts(){
        this.mainMenu.setVisible(false);
        this.viewProduct.setVisible(true);
        this.viewClient.setVisible(false);
        this.viewOrder.setVisible(false);
        DefaultTableModel model = productBLL.getProductTable(viewProduct.getModel());
        viewProduct.getProductTable().setModel(model);
    }

    /**
     * Deschide interfata Orders si afiseaza informatiile necesare din baza de date in tabele.
     */
    public void openOrders(){
        this.mainMenu.setVisible(false);
        this.viewProduct.setVisible(false);
        this.viewClient.setVisible(false);
        this.viewOrder.setVisible(true);

        DefaultTableModel modelClient = clientBLL.getClientsTable(viewOrder.getModelClient());
        viewOrder.getClientsTable().setModel(modelClient);

        DefaultTableModel modelProduct = productBLL.getProductTable(viewOrder.getModelProduct());
        viewOrder.getProductsTable().setModel(modelProduct);

        DefaultTableModel modelOrder = ordersBLL.getOrdersTable(viewOrder.getModelOrder());
        viewOrder.getOrdersTable().setModel(modelOrder);
    }


}
