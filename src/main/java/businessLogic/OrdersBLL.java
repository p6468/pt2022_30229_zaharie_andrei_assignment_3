package businessLogic;

import com.itextpdf.text.*;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import dao.ClientDAO;
import dao.OrderDAO;
import dao.ProductDAO;
import model.Client;
import model.Orders;
import model.Product;
import start.Reflection;

import javax.swing.table.DefaultTableModel;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Zaharie Andrei
 * @Class: OrderBLL - se ocupa cu logica aplicatiei pentru crearea, stergerea si cautarea de comenzi noi. De asemenea genereaza si fisiere PDF pentru comenzile selectate.
 */
public class OrdersBLL {
 private ProductDAO productDAO;
 private OrderDAO orderDAO;
 private ClientDAO clientDAO;

 public OrdersBLL(){
     this.clientDAO = new ClientDAO();
     this.orderDAO = new OrderDAO();
     this.productDAO = new ProductDAO();
 }

    /**
     * Metoda primeste ca si parametru un model de tabel deja existent pe care il modifica cu noile date modificate din baza de date.
     * @param model
     * @return DefaultTableModel
     */
    public DefaultTableModel getOrdersTable(DefaultTableModel model){
        int i;
        int nrRows = model.getRowCount();
        for(int j = nrRows -1; j >=0; j--){
            model.removeRow(j);
        }
        Object line[]= new Object[5];
        List<Orders> ordersList = this.orderDAO.findAllOrders();
        for(Orders c:ordersList) {
            i = 0;
            List<Object> valuesList = Reflection.retrieveProperties(c);
            // System.out.println(valuesList.size());
            for(Object value:valuesList){
                line[i++] = value;
            }
            model.addRow(line);
        }
        return model;
    }

    /**
     * Adauga o noua comanda folosind ca si parametrii clientul care face comanda, produsul pe care doreste sa il comanda si cantitatea dorita din tipul de produs.
     * Modifica stockul pentru acel produs in functie de cantitatea cumparat si mentine baza de date updatata.
     * @param client
     * @param product
     * @param cantitate
     */
    public void addOrder(Client client, Product product, int cantitate){
     double totalPrice = cantitate * product.getProductPrice();
     Orders order = new Orders(client.getIdClient(),product.getIdProduct(),totalPrice,cantitate);
     this.orderDAO.insert(order);
     int stockRamas = product.getProductStock() - cantitate;
     product.setProductStock(stockRamas);
     this.productDAO.update(product);
}

    /**
     * Sterge o comanda prin apelarea functiei delete(id) din orderDAO.
     * @param id
     */
    public void deleteOrder(int id){
     this.orderDAO.deleteOrders(id);
}

    /**
     * Cauta o anumita comanda folosind ca si criteriu de cautare id-ul comenzii si le afiseaza in tabelul din interfata grafica.
     * @param id
     * @param model
     * @return
     */
    public DefaultTableModel searchOrder(int id,DefaultTableModel model){

        int i=0;
        int nrRows = model.getRowCount();
        for(int j = nrRows -1; j >=0; j--){
            model.removeRow(j);
        }
        Object line[]= new Object[6];
        Orders order = this.orderDAO.searchOrders(id);
        List<Object> valuesList = Reflection.retrieveProperties(order);
        // System.out.println(valuesList.size());
        for(Object value:valuesList){
            line[i++] = value;
        }
        model.addRow(line);
        return model;
    }

    /**
     * Genereaza un fisier PDF pentru comanda selectata de catre utilizator si o salveaza intr-un folder predestinat. Fisierul contine date utile despre comanda, cum ar fi
     * datele clientului care a facut comanda, tipul produsului, producatorul, cantitatea comandata si pretul total al comenzii.
     * @param order
     */
    public void generatePDF(Orders order){
        Client client = this.clientDAO.searchClient(order.getIdClient());
        Product product = this.productDAO.searchProduct(order.getIdProduct());

        String imgPath = "D:\\Facultate\\Tehnici PROGRAMARE\\PT2022_30229_Zaharie_Andrei_Assignment_3\\src\\main\\resources\\ZahaShop.png";


        Document document = new Document();
        try {
            String pdfName = "order_" + order.getIdOrder()+ ".pdf";
            String path = "D:\\Facultate\\Tehnici PROGRAMARE\\PT2022_30229_Zaharie_Andrei_Assignment_3\\PDF_Orders\\"+ pdfName;
            PdfWriter writer =  PdfWriter.getInstance(document, new FileOutputStream(path));
            document.open();
            Font font = FontFactory.getFont(FontFactory.COURIER, 16, BaseColor.BLACK);
            Paragraph titlu = new Paragraph("Order"+ order.getIdOrder()+ " Informations.",font);
            titlu.setAlignment(1);
            document.add(titlu);

            PdfPTable productInfo = new PdfPTable(5);
            productInfo.setWidthPercentage(90);
            productInfo.setSpacingBefore(11f);
            productInfo.setSpacingAfter(11f);
            float[] colWidth = {2f,2f,2f,2f,2f};
            productInfo.setWidths(colWidth);
            PdfPCell prodName = new PdfPCell(new Paragraph("Product Name"));
            PdfPCell producer = new PdfPCell(new Paragraph("Producer"));
            PdfPCell cantitate = new PdfPCell(new Paragraph("Product Amount"));
            PdfPCell pricePerUnit = new PdfPCell(new Paragraph("Price Per Unit"));
            PdfPCell totalPrice = new PdfPCell(new Paragraph("Total Price"));

            PdfPCell c1 = new PdfPCell(new Paragraph(product.getProductName()));
            PdfPCell c2 = new PdfPCell(new Paragraph(product.getProductProducer()));
            PdfPCell c3 = new PdfPCell(new Paragraph(order.getCantitate()+""));
            PdfPCell c4 = new PdfPCell(new Paragraph(product.getProductPrice()+" RON"));
            PdfPCell c5 = new PdfPCell(new Paragraph(order.getTotalPrice()+" RON"));

            productInfo.addCell(prodName);
            productInfo.addCell(producer);
            productInfo.addCell(cantitate);
            productInfo.addCell(pricePerUnit);
            productInfo.addCell(totalPrice);
            productInfo.addCell(c1);
            productInfo.addCell(c2);
            productInfo.addCell(c3);
            productInfo.addCell(c4);
            productInfo.addCell(c5);

            document.add(productInfo);

            document.add(new Paragraph("Client Informations: ",font));
            com.itextpdf.text.List clientList = new com.itextpdf.text.List(com.itextpdf.text.List.ORDERED);
            clientList.add("Client id: "+client.getIdClient());
            clientList.add("Client name: "+client.getClientName());
            clientList.add("Client address: "+client.getClientAddress());
            clientList.add("Client Phone Number: "+client.getPhoneNumber());
            clientList.add("Client Email Address: "+client.getEmailAddress());
            document.add(clientList);
            Image shopImg = Image.getInstance(imgPath);
            document.add(new Paragraph("Thank you for buying from Zaha Shop!"));
            document.add(shopImg);

        } catch (DocumentException e) {
            e.printStackTrace();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        document.close();
    }

}
