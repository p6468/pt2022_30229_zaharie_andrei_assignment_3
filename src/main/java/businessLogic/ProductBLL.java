package businessLogic;

import dao.AbstractDAO;
import dao.ProductDAO;
import model.Product;
import model.Product;
import start.Reflection;

import javax.swing.table.DefaultTableModel;
import java.util.List;
import java.util.NoSuchElementException;

/**
 * @author Zaharie Andrei
 * @Class: ProductBLL - aceasta clasa se ocupa cu logica aplicatiti pentru adaguarea,cautarea, modificarea si stergerea de produse din baza de date.
 */
public class ProductBLL {
    private ProductDAO productDAO;
    public ProductBLL(){
        productDAO = new ProductDAO();
    }

    /**
     * Primeste ca si parametru modelul actual al tabelului din interfata grafica pe care il modifica cu noile date schimbate din baza de date.
     * @param model
     * @return DefaultTableModel
     */
    public DefaultTableModel getProductTable(DefaultTableModel model){
        int i;
        int nrRows = model.getRowCount();
        for(int j = nrRows -1; j >=0; j--){
            model.removeRow(j);
        }
        Object line[]= new Object[5];
        List<Product> productsList = this.productDAO.findAllProducts();
        for(Product c:productsList) {
            i = 0;
            List<Object> valuesList = Reflection.retrieveProperties(c);
            // System.out.println(valuesList.size());
            for(Object value:valuesList){
                line[i++] = value;
            }
            model.addRow(line);
        }
        return model;
    }

    /**
     *Insereaza un nou produs in baza de date pentru care apeleaza metoda de insert din productDAO.
     * @param product
     */
    public void insertProduct(Product product){
        productDAO.insert(product);
    }

    /**
     * Modifica datele unui produs dupa care ii da update in baza de date. Aceasta functie apeleaza metoda update(Product) din productDAO.
     * @param product
     */
    public void updateProduct(Product product){
        productDAO.update(product);
    }

    /**
     * Sterge un produs din baza de date. Aceasta functie apeleaza metoda delete(id) din productDAO.
     * @param id
     */
    public void deleteProduct(int id){
        productDAO.deleteProduct(id);
    }

    /**
     * Cauta un produs din baza de date folosind ca metoda de cautare id-ul acestuia si afiseaza in tabelul de pe interfata grafica doar datele care se potrivesc cerintei.
     * @param id
     * @param model
     * @return DefaultTableModel
     */
    public DefaultTableModel searchProduct(int id,DefaultTableModel model){

        int i=0;
        int nrRows = model.getRowCount();
        for(int j = nrRows -1; j >=0; j--){
            model.removeRow(j);
        }
        Object line[]= new Object[6];
        Product product = this.productDAO.searchProduct(id);
        List<Object> valuesList = Reflection.retrieveProperties(product);
        // System.out.println(valuesList.size());
        for(Object value:valuesList){
            line[i++] = value;
        }
        model.addRow(line);
        return model;
    }

}
