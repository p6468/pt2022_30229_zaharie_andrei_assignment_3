package businessLogic;

import dao.AbstractDAO;
import dao.ClientDAO;
import model.Client;
import presentation.GUI.ViewClient;
import start.Reflection;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.util.List;

/**
 * @author Zaharie Andrei
 * @Class: ClientBLL - se ocupa cu logica aplicatiei pentru adaugarea, cautarea, stergerea si modificarea datelor pentru fiecare client in parte
 */
public class ClientBLL {
    private ClientDAO clientDAO;
    public ClientBLL(){
        clientDAO = new ClientDAO();
    }

    /**
     * Metoda primeste ca si parametru un model de tabel deja existent pe care il modifica cu noile date schimbate din baza de date.
     * @param model
     * @return DefaultTableModel
     */
    public DefaultTableModel getClientsTable(DefaultTableModel model){
        int i;
        int nrRows = model.getRowCount();
        for(int j = nrRows -1; j >=0; j--){
            model.removeRow(j);
        }
        Object line[]= new Object[6];
        List<Client> clientsList = this.clientDAO.findAllClients();
        for(Client c:clientsList) {
            i = 0;
            List<Object> valuesList = Reflection.retrieveProperties(c);
           // System.out.println(valuesList.size());
            for(Object value:valuesList){
                    line[i++] = value;
            }
            model.addRow(line);
        }
        return model;
    }

    /**
     * Insereaza un nou client in baza de date pentru care apeleaza metoda de insert din clientDAO.
     * @param client
     */
    public void insertClient(Client client){
        clientDAO.insert(client);
    }

    /**
     * Modifica datele unui client dupa care ii da update in baza de date. Aceasta functie apeleaza metoda update(Client) din clientDAO.
     * @param client
     */
    public void updateClient(Client client){
        clientDAO.update(client);
    }

    /**
     * Sterge un client din baza de date. Aceasta functie apeleaza metoda delete(id) din clientDAO.
     * @param id
     */
    public void deleteClient(int id){
        clientDAO.deleteClient(id);
    }

    /**
     * Cauta un client din baza de date folosind ca metoda de cautare id-ul acestuia si afiseaza in tabelul de pe interfata grafica doar datele care se potrivesc cerintei.
     * @param id
     * @param model
     * @return DefaultTableModel
     */
    public DefaultTableModel searchClient(int id,DefaultTableModel model){

        int i=0;
        int nrRows = model.getRowCount();
        for(int j = nrRows -1; j >=0; j--){
            model.removeRow(j);
        }
        Object line[]= new Object[6];
        Client client = this.clientDAO.searchClient(id);
        List<Object> valuesList = Reflection.retrieveProperties(client);
        // System.out.println(valuesList.size());
        for(Object value:valuesList){
            line[i++] = value;
        }
        model.addRow(line);
        return model;
    }
}
