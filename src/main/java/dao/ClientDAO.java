package dao;

import java.sql.*;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import connection.ConnectionFactory;
import model.Client;

/**
 * @author Zaharie Andrei
 * @group: 30229
 * @Since: 11.04.2022
 * @Class: ClientDAO - este o subcalasa care implementeaza metodele generice din clasa AbstractDAO
 */
public class ClientDAO extends AbstractDAO<Client>{

    /**
     * Metoda implimenteaza functia insert(T) din clasa parinte AbstractDAO si o particularizeaza pentru operatiile necesare din tabela client a bazei de date.
     * @param client
     * @return client
     */
    public Client insert(Client client){
        super.insert(client);
        return client;
    }

    /**
     * Metoda implimenteaza functia update(T) din clasa parinte AbstractDAO si o particularizeaza pentru operatiile necesare din tabela client a bazei de date.
     * @param client
     * @return client
     */
    public Client update(Client client){
        super.update(client);
        return client;

    }

    /**
     * Metoda implimenteaza functia findAll() din clasa parinte AbstractDAO si o particularizeaza pentru operatiile necesare din tabela client a bazei de date.
     * @return List<Client></>
     */
    public List<Client> findAllClients(){
       return super.findAll();
    }

    /**
     * Metoda implimenteaza functia delete(id,idName) din clasa parinte AbstractDAO si o particularizeaza pentru operatiile necesare din tabela client a bazei de date.
     * @param id
     */
    public void deleteClient(int id){
        super.delete(id,"idClient");
    }

    /**
     * Metoda implimenteaza functia findById(id,idName) din clasa parinte AbstractDAO si o particularizeaza pentru operatiile necesare din tabela client a bazei de date.
     * @param id
     * @return client
     */
    public Client searchClient(int id){
        return super.findById(id,"idClient");
    }



}
