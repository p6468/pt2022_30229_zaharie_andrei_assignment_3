package dao;

import model.Orders;
import model.Orders;

import java.util.List;
/**
 * @author Zaharie Andrei
 * @group: 30229
 * @Since: 11.04.2022
 * @Class: OrderDAO - este o subcalasa care implementeaza metodele generice din clasa AbstractDAO
 */
public class OrderDAO extends AbstractDAO<Orders>{
    /**
     * Metoda implimenteaza functia insert(T) din clasa parinte AbstractDAO si o particularizeaza pentru operatiile necesare din tabela Orders a bazei de date.
     * @param order
     * @return order
     */
    public Orders insert(Orders order){
        super.insert(order);
        return order;
    }

    /**
     * Metoda implimenteaza functia update(T) din clasa parinte AbstractDAO si o particularizeaza pentru operatiile necesare din tabela Orders a bazei de date.
     * @param order
     * @return order
     */
    public Orders update(Orders order){
        super.update(order);
        return order;

    }

    /**
     * Metoda implimenteaza functia findAll() din clasa parinte AbstractDAO si o particularizeaza pentru operatiile necesare din tabela Orders a bazei de date.
     * @return List<Orders></>
     */
    public List<Orders> findAllOrders(){
        List<Orders> list = super.findAll();

        for(Orders ord: list){
            if(ord.getCantitate() == 0){
                deleteOrders(ord.getIdOrder());
            }
        }
        return list;
    }

    /**
     * Metoda implimenteaza functia delete(id,idName) din clasa parinte AbstractDAO si o particularizeaza pentru operatiile necesare din tabela Orders a bazei de date.
     * @param id
     */
    public void deleteOrders(int id){
        super.delete(id,"idOrder");
    }

    /**
     * Metoda implimenteaza functia findById(id,idName) din clasa parinte AbstractDAO si o particularizeaza pentru operatiile necesare din tabela Orders a bazei de date.
     * @param id
     * @return order
     */
    public Orders searchOrders(int id){
        return super.findById(id,"idOrder");
    }

}
