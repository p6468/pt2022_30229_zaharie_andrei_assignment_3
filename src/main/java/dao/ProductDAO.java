package dao;

import model.Client;
import model.Product;

import java.util.List;
/**
 * @author Zaharie Andrei
 * @group: 30229
 * @Since: 11.04.2022
 * @Class: ProductDAO - este o subcalasa care implementeaza metodele generice din clasa AbstractDAO
 */
public class ProductDAO extends AbstractDAO<Product>{
    /**
     * Metoda implimenteaza functia insert(T) din clasa parinte AbstractDAO si o particularizeaza pentru operatiile necesare din tabela Product a bazei de date.
     * @param product
     * @return product
     */
    public Product insert(Product product){
        List<Product> list = super.findAll();
        for(Product prod : list){
            if(prod.getIdProduct() == product.getIdProduct()){
                product.setProductStock(product.getProductStock() + prod.getProductStock());
                update(product);
                return product;
            }
        }
        super.insert(product);
        return product;
    }

    /**
     * Metoda implimenteaza functia update(T) din clasa parinte AbstractDAO si o particularizeaza pentru operatiile necesare din tabela Product a bazei de date.
     * @param product
     * @return product
     */
    public Product update(Product product){
        super.update(product);
        return product;

    }

    /**
     * Metoda implimenteaza functia findAll() din clasa parinte AbstractDAO si o particularizeaza pentru operatiile necesare din tabela Product a bazei de date.
     * @return List
     */
    public List<Product> findAllProducts(){
       List<Product> list = super.findAll();

       for(Product prod : list){
           if(prod.getProductStock() == 0){
               deleteProduct(prod.getIdProduct());
           }
       }

       return list;
    }

    /**
     * Metoda implimenteaza functia delete(id,idName) din clasa parinte AbstractDAO si o particularizeaza pentru operatiile necesare din tabela Product a bazei de date.
     * @param id
     */
    public void deleteProduct(int id){
        super.delete(id,"idProduct");
    }

    /**
     * Metoda implimenteaza functia findById(id,idName) din clasa parinte AbstractDAO si o particularizeaza pentru operatiile necesare din tabela Product a bazei de date.
     * @param id
     * @return product
     */
    public Product searchProduct(int id){
        return super.findById(id,"idProduct");
    }
}
