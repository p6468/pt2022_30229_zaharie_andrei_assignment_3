package dao;

import java.beans.IntrospectionException;
import java.beans.PropertyDescriptor;
import java.lang.reflect.*;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import connection.ConnectionFactory;

/**
 * @author Zaharie Andrei
 * @group: 30229
 * @Since: 11.04.2022
 * @Class: AbstractDAO - reprezinta metodele de interactiune a aplicatiei cu baza de date. Acestea sunt abstractizate folosind tipuri generice.
 * Aceasta metoda de a lucra cu date se numeste Reflection.
 */
public class AbstractDAO<T> {
    protected static final Logger LOGGER = Logger.getLogger(AbstractDAO.class.getName());

    private final Class<T> type;

    @SuppressWarnings("unchecked")
    public AbstractDAO() {
        this.type = (Class<T>) ((ParameterizedType) getClass().getGenericSuperclass()).getActualTypeArguments()[0];
    }
    private String createSelectQuery(String field) {
        StringBuilder sb = new StringBuilder();
        sb.append("SELECT ");
        sb.append(" * ");
        sb.append(" FROM ");
        sb.append(type.getSimpleName());
        sb.append(" WHERE " + field + " =?");
        return sb.toString();
    }

    /**
     * Gaseste toate datele ce apartin de un anumit tabel din baza de date si le returneaza sub forma de Lista de obiecte.
     * Aceasta lista poate fi convertita intr-o lista de obiecte de tipul client, product sau order.
     * @return List
     */
    public List<T> findAll() {
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;

        StringBuilder sb = new StringBuilder();
        sb.append("SELECT * FROM ");
        sb.append(type.getSimpleName());
        try {
            connection = ConnectionFactory.getConnection();
            statement = connection.prepareStatement(sb.toString());
            resultSet = statement.executeQuery();
            //System.out.println(resultSet.toString());
            return createObjects(resultSet);
        } catch (SQLException e) {
            LOGGER.log(Level.WARNING, type.getName() + "DAO:findAll " + e.getMessage());
        } finally {
            ConnectionFactory.close(resultSet);
            ConnectionFactory.close(statement);
            ConnectionFactory.close(connection);
        }
        return null;
    }

    public T findByName(String name,String nameFiled){
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        String query = createSelectQuery(nameFiled);
        try {
            connection = ConnectionFactory.getConnection();
            statement = connection.prepareStatement(query);
            statement.setString(1, name);
            resultSet = statement.executeQuery();
            return createObjects(resultSet).get(0);
        } catch (SQLException e) {
            LOGGER.log(Level.WARNING, type.getName() + "DAO:findById " + e.getMessage());
        } finally {
            ConnectionFactory.close(resultSet);
            ConnectionFactory.close(statement);
            ConnectionFactory.close(connection);
        }
        return null;
    }

    /**
     * Cauta un anumit obiect din baza de date dupa id-ul acestuia. Metoda primeste ca parametrii id-ul ce trebuie gasit si idName care reprezinta numele coloanei unde este specificat id-ul
     * @param id
     * @param idName
     * @return List
     */
    public T findById(int id,String idName) {
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        String query = createSelectQuery(idName);
        try {
            connection = ConnectionFactory.getConnection();
            statement = connection.prepareStatement(query);
            statement.setInt(1, id);
            resultSet = statement.executeQuery();

            return createObjects(resultSet).get(0);
        } catch (SQLException e) {
            LOGGER.log(Level.WARNING, type.getName() + "DAO:findById " + e.getMessage());
        } finally {
            ConnectionFactory.close(resultSet);
            ConnectionFactory.close(statement);
            ConnectionFactory.close(connection);
        }
        return null;
    }

    private List<T> createObjects(ResultSet resultSet) {
        List<T> list = new ArrayList<T>();
        Constructor[] ctors = type.getDeclaredConstructors();
        Constructor ctor = null;
        for (int i = 0; i < ctors.length; i++) {
            ctor = ctors[i];
            if (ctor.getGenericParameterTypes().length == 0)
                break;
        }
        try {
            while (resultSet.next()) {
                ctor.setAccessible(true);
                T instance = (T)ctor.newInstance();
                for (Field field : type.getDeclaredFields()) {
                    String fieldName = field.getName();
                        // System.out.println(fieldName);
                        Object value = resultSet.getObject(fieldName);
                        PropertyDescriptor propertyDescriptor = new PropertyDescriptor(fieldName, type);
                        Method method = propertyDescriptor.getWriteMethod();
                        method.invoke(instance, value);

                }
                list.add(instance);
            }
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (SecurityException e) {
            e.printStackTrace();
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (IntrospectionException e) {
            e.printStackTrace();
        }
        return list;
    }

    /**
     * Metoda primeste ca si parametru un obiect generic T pe care il insereaza in baza de date.
     * Tabela unde urmeaza sa fie inserat obiectul este reprezentata de obiectul pentru care este invocata metoda generica.
     * @param t
     * @return genericType
     */
    public T insert(T t) {
        Connection connection = null;
        PreparedStatement statement = null;
        StringBuilder params = new StringBuilder();
        int noFields = t.getClass().getDeclaredFields().length;

        params.append("INSERT INTO ");
        params.append(type.getSimpleName());
        params.append("(");
        for(int index = 1; index < noFields; index++){
            Field field = t.getClass().getDeclaredFields()[index];
            params.append(field.getName());
            if(index != noFields - 1){
                params.append(",");
            }
        }
        params.append(")");
        params.append(" VALUES(");

        for(int index = 1; index < noFields; index++){
            Field field = t.getClass().getDeclaredFields()[index];
            field.setAccessible(true);
            Object value;
            try {
                value = field.get(t);
                String valueStr = String.valueOf(value);
                valueStr = "\"" + valueStr + "\"";
                params.append(valueStr);
                if(index != noFields - 1){
                    params.append(String.valueOf(","));
                }

            } catch (IllegalArgumentException e) {
                e.printStackTrace();
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }
        }
        params.append(")");
        String query = params.toString();
        System.out.println(query);
        try {
            connection = ConnectionFactory.getConnection();
            statement = connection.prepareStatement(query);
            statement.executeUpdate();
            return t;

        } catch (SQLException e) {
            LOGGER.log(Level.WARNING, type.getName() + "DAO:insert" + e.getMessage());
        } finally {
            ConnectionFactory.close(statement);
            ConnectionFactory.close(connection);
        }
        return null;
    }

    /**
     * Metoda sterge din baza de date obiectul cu id-ul primit ca si parametru din coloana reprezentata de catre parametru idName.
     * Tabela din care urmeaza sa fie stersa informatia este reprezentata de catre obiectul pentru care este invocata metoda generica.
     * @param id
     * @param idName
     */
    public void delete(int id, String idName){
        Connection connection = null;
        PreparedStatement statement = null;
        StringBuilder params = new StringBuilder();
        params.append("DELETE FROM ");
        params.append(type.getSimpleName());
        params.append(" WHERE ");
        params.append(idName);
        params.append(" = ");
        params.append(id);
        try {
            connection = ConnectionFactory.getConnection();
            statement = connection.prepareStatement(params.toString());
            statement.executeUpdate();
        } catch (SQLException e) {
            LOGGER.log(Level.WARNING, type.getName() + "DAO:insert" + e.getMessage());
        } finally {
            ConnectionFactory.close(statement);
            ConnectionFactory.close(connection);
        }

    }

    /**
     * Metoda primeste ca si parametru un obiect generic T pe care il modifica in baza de date.
     * Tabela de care apartine informatia ce trebuie modificata este reprezentata de obiectul pentru care este invocata metoda generica.
     * @param t
     * @return genericType
     */
    public T update(T t) {
        Connection connection = null;
        PreparedStatement statement = null;
        StringBuilder params = new StringBuilder();
        int nrFields = t.getClass().getDeclaredFields().length;
        params.append("UPDATE ");
        params.append(type.getSimpleName());
        params.append(" SET ");

        for(int i = 1; i < nrFields; i++){
            Field field = t.getClass().getDeclaredFields()[i];
            field.setAccessible(true);
            params.append(field.getName());
            params.append("=");

            Object value ;
            try {
                value = field.get(t);
                String val ="\"" + value.toString() + "\"";
                params.append(val);
                if(i < nrFields - 1){
                    params.append(", ");
                }
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }
        }
        try {

            Field field = t.getClass().getDeclaredFields()[0];
            field.setAccessible(true);
            Object id = field.get(t);
            String idName = field.getName();
            params.append(" WHERE ");
            params.append(idName);
            params.append(" = ");
            params.append(id.toString());
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }

        try {
            connection = ConnectionFactory.getConnection();
            statement = connection.prepareStatement(params.toString());
            statement.executeUpdate();
            return t;

        } catch (SQLException e) {
            LOGGER.log(Level.WARNING, type.getName() + "DAO:insert" + e.getMessage());
        } finally {
            ConnectionFactory.close(statement);
            ConnectionFactory.close(connection);
        }
        return t;
    }
}