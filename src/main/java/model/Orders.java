package model;
/**
 * @author Zaharie Andrei
 * @group: 30229
 * @Since: 11.04.2022
 * @Class: Orders - Reprezinta implementarea in aplicatie a tabelei Orders din baza de date.
 *  Aceasta clasa contine in mare parte gettere si settere pentru atributele clasei si 2 constructori.
 */
public class Orders {

    private int idOrder;
    private int idClient;
    private int idProduct;
    private int cantitate;
    private double totalPrice;
    public Orders(){

    }
    public Orders(int idClient, int idProduct, double totalPrice, int cantitate){
        this.idClient = idClient;
        this.idProduct = idProduct;
        this.totalPrice = totalPrice;
        this.cantitate = cantitate;

    }

    public int getIdOrder() {
        return idOrder;
    }

    public void setIdOrder(int idOrder) {
        this.idOrder = idOrder;
    }

    public int getIdClient() {
        return idClient;
    }

    public void setIdClient(int idClient) {
        this.idClient = idClient;
    }

    public int getIdProduct() {
        return idProduct;
    }

    public void setIdProduct(int idProduct) {
        this.idProduct = idProduct;
    }

    public double getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(double totalPrice) {
        this.totalPrice = totalPrice;
    }

    public int getCantitate() {
        return cantitate;
    }

    public void setCantitate(int cantitate) {
        this.cantitate = cantitate;
    }
}
