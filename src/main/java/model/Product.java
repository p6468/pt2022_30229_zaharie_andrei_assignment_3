package model;
/**
 * @author Zaharie Andrei
 * @group: 30229
 * @Since: 11.04.2022
 * @Class: Product - Reprezinta implementarea in aplicatie a tabelei Product din baza de date.
 *  *  Aceasta clasa contine in mare parte gettere si settere pentru atributele clasei si 2 constructori.
 */
public class Product {
    private int idProduct;
    private String productName;
    private String productProducer;
    private double productPrice;
    private int productStock;

    public Product(){

    }
    public Product(String productName, String productProducer, double productPrice, int productStock){
        this.productName = productName;
        this.productProducer = productProducer;
        this.productPrice = productPrice;
        this.productStock = productStock;
    }
    public Product(int idProduct,String productName, String productProducer, double productPrice, int productStock){
        this.idProduct = idProduct;
        this.productName = productName;
        this.productProducer = productProducer;
        this.productPrice = productPrice;
        this.productStock = productStock;
    }

    public int getIdProduct() {
        return idProduct;
    }

    public void setIdProduct(int idProduct) {
        this.idProduct = idProduct;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getProductProducer() {
        return productProducer;
    }

    public void setProductProducer(String productProducer) {
        this.productProducer = productProducer;
    }

    public double getProductPrice() {
        return productPrice;
    }

    public void setProductPrice(double productPrice) {
        this.productPrice = productPrice;
    }

    public int getProductStock() {
        return productStock;
    }

    public void setProductStock(int productStock) {
        this.productStock = productStock;
    }
}
