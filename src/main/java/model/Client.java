package model;
/**
 * @author Zaharie Andrei
 * @group: 30229
 * @Since: 11.04.2022
 * @Class: Client - Reprezinta implementarea in aplicatie a tabelei Client din baza de date.
 * Aceasta clasa contine in mare parte gettere si settere pentru atributele clasei si 3 constructori.
 */
public class Client {
    private int idClient;
    private String clientName;
    private String clientAddress;
    private String emailAddress;
    private String phoneNumber;
    private int age;

    public Client(){

    }

    public Client(String clientName, String clientAddress , String emailAddress, String phoneNumber, int age){
        this.clientName = clientName;
        this.clientAddress = clientAddress;
        this.emailAddress = emailAddress;
        this.phoneNumber = phoneNumber;
        this.age = age;
    }
    public Client(int idClient,String clientName, String clientAddress , String emailAddress, String phoneNumber, int age){
        this.idClient = idClient;
        this.clientName = clientName;
        this.clientAddress = clientAddress;
        this.emailAddress = emailAddress;
        this.phoneNumber = phoneNumber;
        this.age = age;
    }

    public int getIdClient() {
        return idClient;
    }

    public void setIdClient(int idClient) {
        this.idClient = idClient;
    }

    public String getClientName() {
        return clientName;
    }

    public void setClientName(String clientName) {
        this.clientName = clientName;
    }

    public String getEmailAddress() {
        return emailAddress;
    }

    public void setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getClientAddress() {
        return clientAddress;
    }

    public void setClientAddress(String clientAddress) {
        this.clientAddress = clientAddress;
    }
    public String toString(){
        return this.idClient + " " + this.clientName + " " + this.clientAddress + " " + this.emailAddress + " "+ this.phoneNumber+" "+ this.age;
    }
}
