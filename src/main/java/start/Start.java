package start;

import businessLogic.ProductBLL;
import model.Client;
import presentation.Controller;

import java.util.logging.Level;
import java.util.logging.Logger;
/**
 * @Author: Zaharie Andrei
 * @group: 30229
 * @Since: 11.04.2022
 * @Class: Start - MainClass pentru pornirea aplicatiei.
 */
public class Start {
    protected static final Logger LOGGER = Logger.getLogger(Start.class.getName());
    public static void main(String[] args) {
        Controller controller = new Controller();
        controller.openMainMenu();


    }
}
