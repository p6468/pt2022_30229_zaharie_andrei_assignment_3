package start;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;
/**
 * @Author: Zaharie Andrei
 * @group: 30229
 * @Since: 11.04.2022
 * @Class: Reflection - Reprezinta modelul de preluare a datelor generice.
 */
public class Reflection {
    /**
     * Preia informatiile unui obiect trimis ca si parametru si returneaza o lista cu acestea.
     * @param object
     * @return List
     */
        public static List<Object> retrieveProperties(Object object) {
            List<Object> list = new ArrayList<>();
            for (Field field : object.getClass().getDeclaredFields()) {
                field.setAccessible(true);
                Object value;
                try {
                    value = field.get(object);
                    list.add(value);
                    System.out.println(field.getName() + "=" + value);

                } catch (IllegalArgumentException e) {
                    e.printStackTrace();
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                }

            }
            return list;
        }
    }

